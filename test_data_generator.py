import random
import string
from pymongo import MongoClient
import radar

client = MongoClient('localhost', 27017)
db = client.test_database
collection = db.test_collection

type_choices = ['read', 'update', 'create', 'delete']

dict_list = []
for count in range(1, 5):
    number = str(random.randint(1000000000000, 9999999999999))
    name = "Пользователь №" + str(count)
    actions_fieldset = []
    for i in range(6):
        actions_fieldset.append({"type": type_choices[random.randint(0, 3)],
                                 "created_at": radar.random_datetime(start='2010-05-24',
                                                                     stop='2017-05-24T23:59:59')})
    session_fieldset = {"created_at": radar.random_datetime(start='2010-05-24', stop='2017-05-24T23:59:59'),
                         "session_id": ''.join(
                             [random.choice(string.ascii_letters + string.digits) for n in range(48)]),
                         "actions": actions_fieldset}
    dict_list.append({"number": number,
                      "name": name,
                      "sessions": session_fieldset
                      })

#            Заполнение тестовой базы данными

# for item in dict_list:
#     pprint.pprint(item)
#     db.test_collection.save(item)

