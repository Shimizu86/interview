import pprint

import pymongo

client = pymongo.MongoClient('localhost', 27017)
db = client.test_database
collection = db.test_collection


def test_query():
    """
     3. Необходимо написать агрегационный запрос, который по каждому пользователю выведет последнее действие
        и общее количество для каждого из типов 'actions'. Итоговые данные должны представлять собой
        список документов вида:

    ##                     ПРИМЕР ВЫВОДА                 ###

    {
           'number': '7800000000000',
           'actions': [
               {
                   'type': 'create',
                   'last': 'created_at': ISODate('2016-01-01T01:33:59'),
                   'count': 12,
               },
               {
                   'type': 'read',
                   'last': 'created_at': ISODate('2016-01-01T01:21:13'),
                   'count': 12,
               },
               {
                   'type': 'update',
                   'last': null,
                   'count': 0,
               },
               {
                   'type': 'delete',
                   'last': null,
                   'count': 0,
               },
           ]
       }
    """
    result = []
    for item in collection.find():
        current = {}
        pipeline = [
            {'$match': {'number': item['number']}},
            {'$project': {'number': 1, '_id': 0, 'actions': "$sessions.actions"}},
            {'$unwind': "$actions"},
            {'$project': {'number': 1,
                          'type': '$actions.type',
                          'count': {'$add': [1]},
                          'created_at': '$actions.created_at'}},
            {'$group': {'_id': "$type", 'count': {'$sum': "$count"}, 'last': {'$max': '$created_at'}}},
        ]
        current['Number'] = item['number']
        current['actions'] = list(collection.aggregate(pipeline))
        result.append(current)
    return result


print(pprint.pprint(test_query()))
