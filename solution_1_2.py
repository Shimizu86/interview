class Metaclass(type):
    """
    1.  Создать мета-класс, определяющий в переменную класса COLLECTION имя коллекции,
        с которой работает создаваемый класс. Имя коллекции должно генерироваться автоматически и
        соответствовать имени класса, написанного с большой буквы.
    """

    def __new__(metacls, name, bases, namespace):
        metaclass = super().__new__(metacls, name, bases, namespace)
        metaclass.COLLECTION = name.capitalize()
        return metaclass


class Account(metaclass=Metaclass):
    """
    2.  На основе созданного мета-класса создать класс с именем 'Account'. При этом экземпляр такого класса
        имеет обязательный строковый атрибут 'number' длиной ровно 13 символов.
    """
    number = 1000000000000

    def __init__(self):
        self.number = str(Account.number)
        Account.number += 1
