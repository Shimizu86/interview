from datetime import datetime
import numpy
import pymongo
import pprint

client = pymongo.MongoClient('localhost', 27017)
db = client.test_database2
accrual = db.test_accrual
payment = db.test_payment


def find_payment(accrual, payment):
    """
    4. Есть две коллекции (таблицы) данных: accrual (долги) и payment (платежи). Обе коллекции имеют поля:
    - id
    - date (дата)
    - month (месяц)
    Необходимо написать функцию, которая сделает запрос к платежам и найдёт для каждого платежа долг, который будет им
    оплачен. Платёж может оплатить только долг, имеющий более раннюю дату. Один платёж может оплатить только один долг,
    и каждый долг может быть оплачен только одним платежом. Платёж приоритетно должен выбрать долг с совпадающим
    месяцем (поле month). Если такого нет, то самый старый по дате (поле date) долг.
    Результатом должна быть таблица найденных соответствий, а также список платежей, которые не нашли себе долг.
    Запрос можно делать к любой базе данных (mongodb, postgresql или другие) любым способом
    """
    accrual_set = {item['id']: {'date': item['date'], 'month': item['month'], 'amount': item['amount']} for item in
                   accrual.find()}
    payment_set = {item['id']: {'date': item['date'], 'month': item['month'], 'amount': item['amount']} for item in
                   payment.find()}
    match_list = []
    free_payment = list(payment_set.keys())
    not_payed_accrual = list(accrual_set.keys())
    for payment_id in payment_set.keys():
        for accrual_id in accrual_set.keys():
            cur_a = accrual_set[accrual_id]
            cur_p = payment_set[payment_id]
            # Отмечаем платежи соответсвующие по количеству и дате долгам
            if numpy.equal(cur_p, cur_a):
                if payment_id not in (id[0] for id in match_list):
                    match_list.append((payment_id, accrual_id))
                    free_payment.remove(payment_id)
                    not_payed_accrual.remove(accrual_id)
            # Отмечаем платежи соответсвующие по количеству и месяцу долгам, но не позже платежа
            elif numpy.array_equal([cur_p['date'].year, cur_p['date'].month, cur_p['amount']],
                                   [cur_a['date'].year, cur_a['date'].month, cur_a['amount']]) and \
                            cur_a['date'] < cur_p['date']:
                match_list.append((payment_id, accrual_id))
                free_payment.remove(payment_id)
                not_payed_accrual.remove(accrual_id)

    for payment_id in free_payment:
        min_date = payment_set[payment_id]['date']
        match_amount = []
        # Находим все долги соответствующие сумме платежа
        for accrual_id in not_payed_accrual:
            if (payment_set[payment_id]['amount'] == accrual_set[accrual_id]['amount']):
                match_amount.append(accrual_set[accrual_id])
        # Вычисляем минимальную дату задолженности
        for item in match_amount:
            if item['date'] < min_date:
                min_date = item['date']

        accrual_without_pay_list = {i: accrual_set[i] for i in not_payed_accrual}
        # Проверяем принадлежит ли минимальная дата какой либо из оставшихся неоплаченными позициям
        for min_accrual_id in accrual_without_pay_list.keys():
            if accrual_without_pay_list[min_accrual_id]['date'] == min_date:
                match_list.append((payment_id, min_accrual_id))
                free_payment.remove(payment_id)
                not_payed_accrual.remove(min_accrual_id)

    # Вывод результата
    print('Оплаченные долги:')
    for item in match_list:
        print("Долг ID:", item[1], accrual_set[item[0]]['date'].strftime('%Y-%m-%d'), '     Сумма:',
              accrual_set[item[1]]['amount'], '    Оплачен   ID: ', item[0], 'Дата: ',
              payment_set[item[0]]['date'].strftime('%Y-%m-%d'))
    print('Неиспользованные платежи:')
    for id in free_payment:
        print('id:', id, '     Дата:   ', payment_set[id]['date'].strftime('%Y-%m-%d'), '    Неиспользованная Сумма: ',
              payment_set[id]['amount'])
    print('Долги:')
    for id in not_payed_accrual:
        print('id:', id, '     Дата:   ', accrual_set[id]['date'].strftime('%Y-%m-%d'), '    Сумма Долга: ',
              accrual_set[id]['amount'])


find_payment(accrual, payment)
