from datetime import datetime
import pymongo

client = pymongo.MongoClient('localhost', 27017)
db = client.test_database2
accrual = db.test_accrual
payment = db.test_payment

accrual_data = ({'id': 1, 'date': datetime(2010, 12, 29), 'month': 12, 'amount': 150},
                {'id': 2, 'date': datetime(2010, 12, 28), 'month': 12, 'amount': 100},
                {'id': 3, 'date': datetime(2010, 10, 25), 'month': 10, 'amount': 120},
                {'id': 4, 'date': datetime(2010, 1, 2), 'month': 1, 'amount': 130},
                {'id': 5, 'date': datetime(2013, 12, 29), 'month': 12, 'amount': 140},
                {'id': 6, 'date': datetime(2010, 12, 29), 'month': 12, 'amount': 140},
                {'id': 7, 'date': datetime(2010, 12, 29), 'month': 12, 'amount': 150},
                {'id': 8, 'date': datetime(2010, 12, 29), 'month': 12, 'amount': 150},
                )
payment_data = ({'id': 1, 'date': datetime(2010, 12, 29), 'month': 12, 'amount': 150},
                {'id': 2, 'date': datetime(2010, 12, 27), 'month': 12, 'amount': 100},
                {'id': 3, 'date': datetime(2010, 10, 26), 'month': 10, 'amount': 120},
                {'id': 5, 'date': datetime(2010, 1, 2), 'month': 1, 'amount': 130},
                {'id': 4, 'date': datetime(2013, 11, 29), 'month': 11, 'amount': 130},
                {'id': 6, 'date': datetime(2010, 10, 29), 'month': 10, 'amount': 140},
                {'id': 7, 'date': datetime(2010, 9, 29), 'month': 9, 'amount': 150},
                {'id': 8, 'date': datetime(2010, 8, 29), 'month': 8, 'amount': 10},
                )
for item in accrual_data:
    accrual.save(item)
for item in payment_data:
    payment.save(item)
